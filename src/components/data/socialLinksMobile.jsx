import instagramLogo from "../../assets/Home/instagramLogo.svg"
import facebookLogo from "../../assets/Home/facebookLogo.svg"
import twitterLogo from "../../assets/Home/twitterLogo.svg"
import youtubeLogo from "../../assets/Home/youtubeLogo.svg"
import ticktokLogo from "../../assets/Home/ticktokLogo.svg"

import instagramImage from "../../assets/Home/instagram.svg"
import facebookImage from "../../assets/Home/facebook.svg"
import twitterImage from "../../assets/Home/twitter.svg"
import youtubeImage from "../../assets/Home/youtube.svg"
import ticktokImage from "../../assets/Home/ticktok.svg"

export const socialLinksMobile = [
    {
        bg_image1 : `${instagramImage}`,
        bg_image2 : `${twitterImage}`,
        src1: `${instagramLogo}`,
        src2: `${twitterLogo}`,
        link1: "INSTAGRAM",
        link2: "TWITTER",
        page_url1: "https://www.instagram.com/nowandforevertx/?hl=en",
        page_url2: "https://twitter.com/nowandforevertx?lang=en",
    },
    {
        bg_image1 : `${facebookImage}`,
        bg_image2 : `${youtubeImage}`,
        src1: `${facebookLogo}`,
        src2: `${youtubeLogo}`,
        link1: "FACEBOOK",
        link2: "YOUTUBE",
        page_url1: "https://www.facebook.com/nowandforevertx/",
        page_url2: "https://www.youtube.com/",
    },
    {
        bg_image1 : `${ticktokImage}`,
        src1: `${ticktokLogo}`,
        link1: "TIKTOK",
        page_url1: "https://www.tiktok.com/@nowandforeverhtx",
    },
]    