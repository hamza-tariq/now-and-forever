import regular_unleaded from "../../assets/Services/regular_unload.png"
import super_unleaded from "../../assets/Services/super_unloaded.png"
import desiel from "../../assets/Services/deisel.png"
import desiel2 from "../../assets/Services/deisel2.png"
import clear_diesel from "../../assets/Services/clear_deisel.png"



export const fuel_service = [
    {
        src: `${regular_unleaded}`,
        head: "Regular Unleaded",
        text1: "- Octane Rating: 87",
        text2: "- Ethanol Percentage: Up to 10%",
        text3: "- Suitable for most gasoline-powered vehicles.",
        text4: "- Helps reduce greenhouse gas emissions and fossil fuel dependence.",
        text5: "- Provides standard engine performance and fuel efficiency.",
        dashed: "1"
    },
    {
        src: `${super_unleaded}`,
        head: "Super Unleaded",
        text1: "- Octane Rating: Typically around 91 or 93",
        text2: "- Ethanol Percentage: Up to 10%",
        text3: "- Ideal for high-performance vehicles or those requiring higher octane levels.",
        text4: "- Prevents engine knock or pinging, optimizing engine performance.",
        text5: "- Offers smoother acceleration and improved throttle response.",
        text6: "- Supports engine longevity by reducing stress and wear on engine components.",
        dashed: "1"
    },
    {
        src: `${desiel}`,
        head: "Diesel",
        text1: "- Octane Rating: 87",
        text2: "- Ethanol Percentage: Up to 10%",
        text3: "- Suitable for most gasoline-powered vehicles.",
        text4: "- Helps reduce greenhouse gas emissions and fossil fuel dependence.",
        text5: "- Provides standard engine performance and fuel efficiency.",
        dashed: "1"
    },
    {
        src: `${desiel2}`,
        head: "Diesel",
        text1: "- Octane Rating: 87",
        text2: "- Ethanol Percentage: Up to 10%",
        text3: "- Suitable for most gasoline-powered vehicles.",
        text4: "- Helps reduce greenhouse gas emissions and fossil fuel dependence.",
        text5: "- Provides standard engine performance and fuel efficiency.",
        dashed: "1"
    },
    {
        src: `${clear_diesel}`,
        head: "Clear Diesel",
        text1: "- Octane Rating: Typically around 91 or 93",
        text2: "- Ethanol Percentage: Up to 10%",
        text3: "- Ideal for high-performance vehicles or those requiring higher octane levels.",
        text4: "- Prevents engine knock or pinging, optimizing engine performance.",
        text5: "- Offers smoother acceleration and improved throttle response.",
        text6: "- Supports engine longevity by reducing stress and wear on engine components.",
        dashed: ""
    },
];