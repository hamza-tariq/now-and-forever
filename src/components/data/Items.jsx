import auto from "../../assets/Home/auto.svg"
import beverages from "../../assets/Home/beverages.svg"
import carwash from "../../assets/Home/carwash.svg"
import food from "../../assets/Home/food.svg"
import fuel from "../../assets/Home/fuel.svg"
import gifts from "../../assets/Home/gifts.svg"
import grocery from "../../assets/Home/grocery.svg"
import smokeshop from "../../assets/Home/smokeshop.svg"
import moneycrypto from "../../assets/Home/money-crypto.svg"

export const Items = [
    {
        src: `${moneycrypto}`,
        heading: "MONEY & CRYPTO SERVICES",
    },
    {
        src: `${auto}`,
        heading: "AUTO",
    },
    {
        src: `${beverages}`,
        heading: "BEVERAGES",
    },
    {
        src: `${gifts}`,
        heading: "GIFTS",
    },
    {
        src: `${grocery}`,
        heading: "GROCERY",
    },
    {
        src: `${fuel}`,
        heading: "FUEL",
    },
    {
        src: `${food}`,
        heading: "FOOD",
    },
    {
        src: `${carwash}`,
        heading: "CAR WASH",
    },
    {
        src: `${smokeshop}`,
        heading: "SMOKE SHOP",
    },
];
