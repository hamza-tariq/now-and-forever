import merchandise_1 from "../../assets/Merch/mer_pic_1.png"
import merchandise_2 from "../../assets/Merch/mer_pic_2.jpg"
export const merchandise_data = [
    {
        img: `${merchandise_1}`,
        title: "Its Boo not Moo Tee",
        price: "$11.99",
        url: "https://nowandforever.com/product/its-boo-not-moo-tee/"
    },
    {
        img: `${merchandise_2}`,
        title: "Te Quiero Moooo-cho Mug",
        price: "$19.99.",
        url: "https://nowandforever.com/product/te-quiero-moooo-cho-mug/"
    },
];