import React from 'react'
import "./Section02.css"
import DesPic from "../../../../../assets/Services/fresh-food-des-pic.png"

const Section02 = () => {
  return (
    <>
     <div className='d-flex flex-column flex-lg-row align-items-center px-2 px-lg-4 mx-auto aboutsecondBanner'>
        <div className='d-flex flex-column align-items-center align-items-lg-start justify-content-center mb-5 mb-lg-0'>
          <div>
            <h1>Texas Forever <br /> Bar and Grill</h1>
            <p className='p-add'>Address</p>
            <p className='pb-3'>Located next to Now and Forever Channelview  17124 E Freeway Service Rd Suite A, Channelview, TX 77530</p>
            <button className="btn btn-sm btn-outline-dark rounded-5 px-5 py-2">Order</button>
          </div>
        </div>
        <div>
          <img className='img-fluid w-100 h-100' src={DesPic} alt="" draggable='false' />
        </div>
      </div>
    </>
  )
}

export default Section02