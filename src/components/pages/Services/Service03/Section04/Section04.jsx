import React from 'react'
import "./Section04.css"
import food_partner from "../../../../../assets/Services/food_partner.png"

const Section04 = () => {
  return (
    <>
    <h1 className='mx-3 text-center sec_main_h'>Our <span>Food Partners</span></h1>
     <div className='d-flex flex-column flex-lg-row align-items-center px-2 px-lg-4 mx-auto services_partners_banner'>
        <div className='d-flex flex-column align-items-center align-items-lg-start justify-content-center mb-5 mb-lg-0'>
          <div>
            <h1>Hunt Bros Pizza</h1>
            <p className='p-add'>Available at</p>
            <p className='p-bold'>Now & Forever Parker</p>
            <p className='pb-3'>- 251 W Parker Rd, Houston, TX 77076</p>
            <p className='p-bold'>Now & Forever Channelview</p>
            <p className='pb-3'>- 17124 E Freeway Service Rd Suite B, Channelview, TX 77530</p>
            <button className="btn btn-sm btn-outline-dark rounded-5 px-5 py-2">Order</button>
          </div>
        </div>
        <div>
          <img className='img-fluid w-100 h-100' src={food_partner} alt="" draggable='false' />
        </div>
      </div>
    </>
  )
}

export default Section04