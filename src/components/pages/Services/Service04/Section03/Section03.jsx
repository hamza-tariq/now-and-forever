import React from 'react'
import "./Section03.css"
import serban from "../../../../../assets/Services/money_ser_ban.png"

const Section03 = () => {
  return (
    <>
     <div className='d-flex flex-column-reverse flex-lg-row align-items-center px-lg-2 service04-Sec03'>
        <div className='d-flex flex-column align-items-center align-items-lg-start justify-content-center mb-5 mt-5 mt-lg-0 mb-lg-0'>
          <div className='w-100'>
            <h1 className='d-none d-lg-block'>Become A <span>KIRV <br /></span>ATM</h1>
            <h1 className='d-block d-lg-none mb-3'>Become A <span>KIRV </span>ATM</h1>
            <p className='m-0'>- Elevate your business with KIRV services.</p>
            <p className='m-0'>- Guaranteed commissions on smart ATM services.</p>
            <p className='m-0'>- One-stop for maintenance, hardware, cash services, and more.</p>
            <p className='mb-3'>- Round-the-clock support with wireless connectivity.</p>
            <p className='text-black-c mb-5'>Experience financial ease at Now and Forever – where simplicity meets innovation.</p>
            <button class="btn btn-sm btn-outline-dark rounded-5 px-5 py-2">Learn More</button>
          </div>
        </div>
        <div>
          <img className='img-fluid w-100 h-100' src={serban} alt="" draggable='false' />
        </div>
      </div>
    </>
  )
}

export default Section03