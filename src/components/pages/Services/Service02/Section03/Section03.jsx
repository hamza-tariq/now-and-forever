import React from 'react'
import "./Section03.css"
import FuelPartnerImage from "../../../../../assets/Services/FuelPartnerImage.svg"

const Section03 = () => {
  return (
    <>
     <div className='d-flex flex-column flex-lg-row align-items-center px-2 px-lg-4 mx-auto serviceBanner'>
        <div className='d-flex flex-column align-items-center align-items-lg-start justify-content-center mb-5 mb-lg-0'>
          <div>
            <h1>Third-<span>Party <br />Fuel </span>Partner</h1>
            <p className='pt-3'>Shell is a global energy and petrochemical leader in over 70 countries.</p>
            <p className='pb-3'>In addition to our own Now and Forever branded fuel, we proudly offer Shell branded fuel at select locations, bringing you trusted quality and reliability. Experience the excellence of Shell's renowned fuels, handpicked to elevate your driving experience. Wherever you go, count on Now and Forever for top-tier fuel options, including Shell.</p>
            <button className="btn btn-sm btn-outline-dark rounded-5 px-3 py-2">Learn More</button>
          </div>
        </div>
        <div>
          <img className='img-fluid w-100 h-100' src={FuelPartnerImage} alt="" draggable='false' />
        </div>
      </div>
    </>
  )
}

export default Section03