import React from 'react'
import "./Section07.css"

const Section07 = () => {
  return (
    <>
      <div className='video-container'>
        <video width="100%" height="100%" muted loop autoPlay>
          <source src="videoHomePage.mp4" type="video/mp4" />
        </video>
      </div>
    </>
  )
}

export default Section07