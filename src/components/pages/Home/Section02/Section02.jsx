import React from 'react'
import { Slider } from '../../../data/Slider'
import sliderarrowleft from "../../../../assets/Home/sliderarrowleft.svg"
import sliderarrowright from "../../../../assets/Home/sliderarrowright.svg"

import './Section02.css'
import { Link } from 'react-router-dom'

const Section02 = () => {
  return (
    <>
      <div id="homeMainSlider" className="carousel slide position-relative slide_desktop">
        <div className="carousel-inner">
          {Slider.map((data, index) => (
            <div key={index} className={`${index === 0 ? "active" : ""} carousel-item`}>
              <img src={data.src} className="d-block w-50" alt="..." draggable='false' />
              <div className="carousel-caption p-0 d-none d-md-block h-100 w-50 carousalCaption">
                <div className='h-100 bg-solid-pink'>
                  <div className='text-white box-des-slider d-flex flex-column align-items-start justify-content-center text-start h-100'>
                    <h2>{data.heading}</h2>
                    <p>{data.text}</p>
                    <Link to='/locations' target='_blank' className='btn btn-outline-light rounded-5 px-3 py-2'>{data.btn_text}</Link>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
        <button className="carousel-control-prev z-0" type="button" data-bs-target="#homeMainSlider" data-bs-slide="prev">
          <img className='' src={sliderarrowleft} alt="" draggable='false' />
        </button>
        <button className="carousel-control-next z-0" type="button" data-bs-target="#homeMainSlider" data-bs-slide="next">
          <img src={sliderarrowright} alt="" draggable='false' />
        </button>
        <div className="carousel-indicators position-static mt-3">
          <button type="button" data-bs-target="#homeMainSlider" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#homeMainSlider" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#homeMainSlider" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
      </div>

      {/* Mobile Header */}

      <div id="carouselExampleCaptions" className="carousel slide slide_mobile">

        <div className="carousel-inner">
          {Slider.map((data, index) => (
            <div key={index} className={`${index === 0 ? "active" : ""} carousel-item`}>
              <img src={data.src} className="d-block w-100" alt="..." draggable='false' />
              <div className="carousel-caption p-0 d-block w-100 carousalCaption">
                <div className='h-100 bg-solid-pink'>
                  <div className='text-white mx-3 box-des-slider d-flex flex-column align-items-center justify-content-center text-center h-100'>
                    <h2>{data.heading}</h2>
                    <p>{data.text}</p>
                    <button className='btn btn-sm btn-outline-light rounded-5 px-3 py-2'>{data.btn_text}</button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>

        {/* <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
          <img width={40} src={sliderarrowleft} alt="" draggable='false' />
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
          <img width={40} src={sliderarrowright} alt="" draggable='false' />
        </button> */}

        <div className="carousel-indicators position-static mt-3">
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
      </div>
    </>
  )
}

export default Section02