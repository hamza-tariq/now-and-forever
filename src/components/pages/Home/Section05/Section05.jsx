import React from 'react'
import handshake from "../../../../assets/Home/handshake.svg"
import "./Section05.css"
import { Link } from 'react-router-dom'

const Section05 = () => {
  return (
    <>
    <div className='location-bg'>
      <div className='m-auto h-100 d-flex flex-column align-items-center align-items-lg-start justify-content-center'>
        <img className='handshakeimg' src={handshake} alt="" draggable='false' />
        <h1 className='text-white my-3 mx-4'>Interested in leasing with us?</h1>
        <Link to='/contact' className='btn btn-sm btn-outline-light rounded-5 px-4 py-2 mx-4'>Learn More</Link>
      </div>
    </div>
    </>
  )
}

export default Section05