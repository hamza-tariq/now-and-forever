import React from 'react'
import "./Section03.css"
import aboutwelcomeimg from "../../../../assets/About/aboutwelcomeimg.svg"

const Section03 = () => {
    return (
        <>
            <div className='bd-sec-3'>
                <div className='d-flex flex-column flex-lg-row align-items-center px-2 px-lg-4 py-2 py-lg-5 mx-auto aboutthirdBanner'>
                    <div className='d-flex flex-column align-items-center align-items-lg-start justify-content-center mb-5 mb-lg-0'>
                        <div className='text-white'>
                            <h1>Welcome to <br /> <span className='px-1 bg-white'>Now & Forever!</span></h1>
                            <p className='pb-3'>We seek to be the first place our community thinks of when they need fuel and other basics We seek to be the first place our community thinks of when they need fuel and other basics</p>
                            <button className="btn btn-sm btn-outline-light rounded-5 px-3 py-2">Learn More</button>
                        </div>
                    </div>
                    <div>
                        <img className='img-fluid w-100 h-100' src={aboutwelcomeimg} alt="" draggable='false' />
                    </div>
                </div>
            </div>  
        </>
    )
}

export default Section03