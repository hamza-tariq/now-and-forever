import React from 'react'
import "./Section02.css"
import whoweImage from "../../../../assets/About/whoweImage.svg"
import whoweImage2 from "../../../../assets/About/whoweImage2.svg"

const Section02 = () => {
  return (
    <>
      <div className='d-flex flex-column flex-lg-row align-items-center px-2 px-lg-4 mx-auto aboutsecondBanner'>
        <div className='d-flex flex-column align-items-center align-items-lg-start justify-content-center mb-5 mb-lg-0'>
          <div>
            <h1>Who <span>We </span>Are</h1>
            <p className='pb-3'>We seek to be the first place our community thinks of when they need fuel and other basics We seek to be the first place our community thinks of when they need fuel and other basics</p>
            <button className="btn btn-sm btn-outline-dark rounded-5 px-3 py-2">Learn More</button>
          </div>
        </div>
        <div>
          <img className='img-fluid w-100 h-100' src={whoweImage} alt="" draggable='false' />
        </div>
      </div>

      <div className='d-flex flex-column-reverse flex-lg-row align-items-center px-2 px-lg-4 mx-auto aboutsecondBanner'>
        <div>
          <img className='img-fluid w-100 h-100' src={whoweImage2} alt="" draggable='false' />
        </div>
        <div className='d-flex flex-column align-items-center align-items-lg-end justify-content-center mb-5 mb-lg-0'>
          <div>
            <h1>How <span>We </span>Work</h1>
            <p className='pb-3'>We seek to be the first place our community thinks of when they need fuel and other basics We seek to be the first place our community thinks of when they need fuel and other basics</p>
            <button className="btn btn-sm btn-outline-dark rounded-5 px-3 py-2">Learn More</button>
          </div>
        </div>
      </div>

    </>
  )
}

export default Section02